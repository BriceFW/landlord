package com.jcdesimp.landlord;

import biz.princeps.lib.storage.AbstractDatabase;
import com.jcdesimp.landlord.configuration.CustomConfig;
import com.jcdesimp.landlord.landFlags.*;
import com.jcdesimp.landlord.landManagement.FlagManager;
import com.jcdesimp.landlord.landManagement.LandManager;
import com.jcdesimp.landlord.landManagement.ViewManager;
import com.jcdesimp.landlord.landMap.MapManager;
import com.jcdesimp.landlord.persistantData.db.MySQLDatabase;
import com.jcdesimp.landlord.persistantData.db.SQLiteDatabase;
import com.jcdesimp.landlord.pluginHooks.WorldguardHandler;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Main plugin class for Landlord
 */
public final class Landlord extends JavaPlugin {

    private AbstractDatabase db;
    private static Landlord plugin;
    private MapManager mapManager;
    private WorldguardHandler wgHandler;
    private FlagManager flagManager;
    private ViewManager manageViewManager;
    private LandManager landManager;
    private static Economy econ = null;

    private CustomConfig messagesConfig;

    public static Landlord getInstance() {
        return plugin;
    }

    public Economy getEcon(){
        return econ;
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    @Override
    public void onEnable() {
        plugin = this;
        mapManager = new MapManager(this);

        flagManager = new FlagManager(this);
        manageViewManager = new ViewManager();
        getServer().getPluginManager().registerEvents(mapManager, this);

        saveDefaultConfig();

        String lang = getConfig().getString("options.messagesFile").replace("/", ".");
        messagesConfig = new CustomConfig(this, "messages/" + lang, "messages/" + lang);

        getServer().getPluginManager().registerEvents(new LandAlerter(plugin), this);

        // database stuff
        if (getConfig().getBoolean("SQLite.enable")) {
            db = new SQLiteDatabase(this.getDataFolder() + "/database.db");
            ((SQLiteDatabase) db).setupDatabase();
        } else
            db = new MySQLDatabase(getConfig().getString("MySQL.Hostname"), getConfig().getInt("MySQL.Port"), getConfig().getString("MySQL.Database"), getConfig().getString("MySQL.User"), getConfig().getString("MySQL.Password"));

        landManager = new LandManager();


        // Command Executor
        getCommand("landlord").setExecutor(new LandlordCommandExecutor(this));

        //Worldguard Check
        if (!hasWorldGuard() && this.getConfig().getBoolean("worldguard.blockRegionClaim", true)) {
            getLogger().warning("Worldguard not found, worldguard features disabled.");
        } else if (hasWorldGuard()) {
            getLogger().info("Worldguard found!");
            wgHandler = new WorldguardHandler(getWorldGuard());
        }

        if (!setupEconomy()) {
            getLogger().warning("Impossible de trouver Vault !");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        this.getServer().getPluginManager().registerEvents(landManager, this);
        //Register default flags
        if (getConfig().getBoolean("enabled-flags.build")) {
            flagManager.registerFlag(new Build(this));
        }
        if (getConfig().getBoolean("enabled-flags.harmAnimals")) {
            flagManager.registerFlag(new HarmAnimals(this));
        }
        if (getConfig().getBoolean("enabled-flags.useContainers")) {
            flagManager.registerFlag(new UseContainers(this));
        }
        if (getConfig().getBoolean("enabled-flags.tntDamage")) {
            flagManager.registerFlag(new TntDamage(this));
        }
        if (getConfig().getBoolean("enabled-flags.useRedstone")) {
            flagManager.registerFlag(new UseRedstone(this));
        }
        if (getConfig().getBoolean("enabled-flags.openDoor")) {
            flagManager.registerFlag(new OpenDoor(this));
        }
        if (getConfig().getBoolean("enabled-flags.pvp")) {
            flagManager.registerFlag(new PVP(this));
        }
        if (getConfig().getBoolean("enabled-flags.setHome")) {
            flagManager.registerFlag(new SetHome(this));
        }
    }

    @Override
    public void onDisable() {
        getLogger().info(getDescription().getName() + " has been disabled!");
        mapManager.removeAllMaps();
        manageViewManager.deactivateAll();
        db.close();
    }

    public FileConfiguration getMessageConfig() {
        return messagesConfig.get();
    }

    public CustomConfig getMessagesConfigCustom() { return messagesConfig; }

    public FlagManager getFlagManager() {
        return flagManager;
    }

    public MapManager getMapManager() {
        return mapManager;
    }

    public ViewManager getManageViewManager() {
        return manageViewManager;
    }

    private WorldGuardPlugin getWorldGuard() {
        Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");

        // WorldGuard may not be loaded
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
            return null; // Maybe you want throw an exception instead
        }

        return (WorldGuardPlugin) plugin;
    }

    public WorldguardHandler getWgHandler() {
        return wgHandler;
    }

    public boolean hasWorldGuard() {
        Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");
        return !(plugin == null || !(plugin instanceof WorldGuardPlugin) || !this.getConfig().getBoolean("worldguard.blockRegionClaim", true));
    }

    public AbstractDatabase getDb() {
        return db;
    }

    public LandManager getLandManager() {
        return landManager;
    }
}
