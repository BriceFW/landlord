package com.jcdesimp.landlord;

import com.jcdesimp.landlord.persistantData.OwnedLand;
import fr.vikingsworld.vikingsworld.Viking;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;
import java.util.UUID;

/**
 * File created by jcdesimp on 4/30/14.
 * Alerts a player when they enter land.
 */
public class LandAlerter implements Listener {

    private HashMap<String, UUID> landIn;
    private HashMap<String, Long> lastChunkChange;
    private Landlord plugin;

    LandAlerter(Landlord plugin) {
        this.plugin = plugin;
        this.landIn = new HashMap<>();
        this.lastChunkChange = new HashMap<>();
    }

    private void showClaimOwner(Player player, Location loc) {
        if (player == null || loc == null) {
            return;
        }

        OwnedLand land = plugin.getLandManager().getLandFromCache(loc);

        // Maj scoreboard
        Scoreboard sb = player.getScoreboard();
        Objective obj = sb.getObjective(DisplaySlot.SIDEBAR);
        if (obj != null && obj.getName().equals("vikingsboard")) {
            for (String line : sb.getEntries()) {
                if (line.contains("Claim")) {
                    sb.resetScores(line);
                    break;
                }
            }
            String landName = "Aucun";
            if (land != null) {
                landIn.put(player.getName(), land.getOwner());
                if (land.getOwner().equals(player.getUniqueId()) || land.isFriend(player.getUniqueId())) {
                    landName = "§a" + land.getOwnerUsername();
                } else {
                    landName = "§c" + land.getOwnerUsername();
                }
            }

            Score claim = obj.getScore("§6Claim : §f" + landName);
            claim.setScore(-4);
            player.setScoreboard(sb);
        } else { // Ou message action bar
            if (landIn.containsKey(player.getName())) {
                UUID prevName = landIn.get(player.getName());
                if (land == null) {
                    if (prevName.equals(player.getUniqueId())) {
                        player.sendActionBar("§a> Vous quittez votre claim <");
                    } else {
                        player.sendActionBar("§a> Vous quittez le claim de " + prevName + " <");
                    }

                } else {
                    if (!prevName.equals(land.getOwner())) {
                        if (prevName.equals(player.getUniqueId())) {
                            player.sendActionBar("§a> Vous quittez votre claim <");
                        } else {
                            player.sendActionBar("§a> Vous quittez le claim de " + new Viking(prevName).getName() + " <");
                        }
                    }
                }
            }

            if (land != null) {
                if (landIn.containsKey(player.getName())) {
                    UUID prevName = landIn.get(player.getName());
                    if (!prevName.equals(land.getOwner())) {
                        landIn.put(player.getName(), land.getOwner());

                        if (land.getOwner().equals(player.getUniqueId())) {
                            player.sendActionBar("§a> Vous entrez dans votre claim <");
                        } else {
                            if (land.isFriend(player.getUniqueId())) {
                                player.sendActionBar("§a> Vous entrez dans le claim de " + land.getOwnerUsername() + " (ami) <");
                            } else {
                                player.sendActionBar("§a> Vous entrez dans le claim de " + land.getOwnerUsername() + " <");
                            }
                        }
                    }
                } else {
                    landIn.put(player.getName(), land.getOwner());
                    if (land.getOwner().equals(player.getUniqueId())) {
                        player.sendActionBar("§a> Vous entrez dans votre claim <");
                    } else {
                        if (land.isFriend(player.getUniqueId())) {
                            player.sendActionBar("§a> Vous entrez dans le claim de " + land.getOwnerUsername() + " (ami) <");
                        } else {
                            player.sendActionBar("§a> Vous entrez dans le claim de " + land.getOwnerUsername() + " <");
                        }
                    }
                }
            }
        }

        // Enlever le fly si on est pu dans un claim
        if (land == null) {
            landIn.remove(player.getName());
            noFly(player);
            return;
        }

        // pareil si pas friend et pas owner
        if (!land.isFriend(player.getUniqueId()) && !land.getOwner().equals(player.getUniqueId())) {
            noFly(player);
        }

        // On active le fly si on est owner ou friend
        if (land.isFriend(player.getUniqueId()) || land.getOwner().equals(player.getUniqueId())) {
            if (player.hasPermission("landlord.fly")) {
                player.setAllowFlight(true);
            }
        }
    }

    private void removeClaimOwner(Player player) {
        Scoreboard sb = player.getScoreboard();
        Objective obj = sb.getObjective(DisplaySlot.SIDEBAR);
        if (obj != null && obj.getName().equals("vikingsboard")) {
            for (String line : sb.getEntries()) {
                if (line.contains("Claim")) {
                    sb.resetScores(line);
                    break;
                }
            }
            Score claim = obj.getScore("§6Claim : §7...");
            claim.setScore(-4);
            player.setScoreboard(sb);
        }
    }

    private void showNoClaim(Player player) {
        Scoreboard sb = player.getScoreboard();
        Objective obj = sb.getObjective(DisplaySlot.SIDEBAR);
        if (obj != null && obj.getName().equals("vikingsboard")) {
            for (String line : sb.getEntries()) {
                if (line.contains("Claim")) {
                    sb.resetScores(line);
                    break;
                }
            }
        }

        noFly(player);
    }

    private void noFly(Player player) {
        if (player.getAllowFlight() && !player.hasPermission("vikings.staff")) {
            if (player.isFlying()) {
                player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100, 5, false, false));
                player.setFlying(false);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', "§7[§6§lClaim >§7]§f Vous ne pouvez plus voler ici."));
            }
            player.setAllowFlight(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void alertOnChunkChange(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        if (!event.getTo().getWorld().getName().equals("world")) {
            showNoClaim(player);
            return;
        }

        if (player.getVehicle() != null)
            return;

        if (!event.getFrom().getChunk().equals(event.getTo().getChunk())) {
            removeClaimOwner(player);
            lastChunkChange.put(player.getName(), System.currentTimeMillis());

            new BukkitRunnable() {
                @Override
                public void run() {
                    if (lastChunkChange.containsKey(player.getName()) && System.currentTimeMillis() - lastChunkChange.get(player.getName()) > 3200) {
                        showClaimOwner(player, player.getLocation());
                    }
                }
            }.runTaskLater(plugin, 65);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void alertOnTeleportation(PlayerTeleportEvent event) {
        if (!event.getTo().getWorld().getName().equals("world")) {
            showNoClaim(event.getPlayer());
            return;
        }

        showClaimOwner(event.getPlayer(), event.getTo());
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void playerLeave(PlayerQuitEvent event) {
        landIn.remove(event.getPlayer().getName());
        lastChunkChange.remove(event.getPlayer().getName());
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        lastChunkChange.put(e.getPlayer().getName(), System.currentTimeMillis());
    }
}
