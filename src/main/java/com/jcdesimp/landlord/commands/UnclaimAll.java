package com.jcdesimp.landlord.commands;

import com.jcdesimp.landlord.Landlord;
import com.jcdesimp.landlord.persistantData.OwnedLand;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by jcdesimp on 2/17/15.
 * LandlordCommand object for players to unclaim land
 */
public class UnclaimAll implements LandlordCommand {

    private Landlord plugin;

    public UnclaimAll(Landlord plugin) {
        this.plugin = plugin;
    }

    /**
     * Called when landlord unclaim command is executed
     * This command must be run by a player
     *
     * @param sender who executed the command
     * @param args   given with command
     * @return boolean
     */
    public boolean execute(CommandSender sender, String[] args, String label) {

        FileConfiguration messages = plugin.getMessageConfig();

        final String notPlayer = messages.getString("info.warnings.playerCommand");
        final String noPerms = messages.getString("info.warnings.noPerms");

        //is sender a player
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.DARK_RED + notPlayer));
        } else {
            Player player = (Player) sender;
            if (!player.hasPermission("landlord.player.own") && !player.hasPermission("landlord.admin.unclaim")) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + noPerms));
                return true;
            }
            List<OwnedLand> landlist = plugin.getDb().getLands(player.getUniqueId());

            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "§7[§6§lClaim >§7]§f Tous vos claims ont été supprimés (" + landlist.size() + ")"));


            for (OwnedLand dbLand : landlist) {
                Chunk currChunk = dbLand.getChunk();

                dbLand.delete();
                OwnedLand.highlightLand(player, Particle.SPELL_WITCH);

                //Regen land if enabled
                if (plugin.getConfig().getBoolean("options.regenOnUnclaim", false)) {
                    currChunk.getWorld().regenerateChunk(currChunk.getX(), currChunk.getZ());
                }

                if (plugin.getConfig().getBoolean("options.soundEffects", true)) {
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_HURT, 10, .5f);
                }
            }
            plugin.getMapManager().updateAll();
        }
        return true;
    }

    public String getHelpText(CommandSender sender) {

        FileConfiguration messages = plugin.getMessageConfig();

        final String usage = messages.getString("commands.unclaimall.usage");            // get the base usage string
        final String desc = messages.getString("commands.unclaimall.description");                        // get the description
        final String regenWarning = messages.getString("commands.unclaimall.alerts.regenWarning");                 // get the chunk regen warning message

        String helpString = "";

        helpString += Utils.helpString(usage, desc, getTriggers()[0].toLowerCase());

        // add chunk regen warning if needed
        if (plugin.getConfig().getBoolean("options.regenOnUnclaim", false)) {
            helpString += ChatColor.RED + " " + ChatColor.ITALIC + regenWarning;
        }

        // return the constructed and colorized help string
        return helpString;
    }

    public String[] getTriggers() {
        final List<String> triggers = plugin.getMessageConfig().getStringList("commands.unclaimall.triggers");
        return triggers.toArray(new String[triggers.size()]);
    }
}
