package com.jcdesimp.landlord.commands;

import com.jcdesimp.landlord.Landlord;
import com.jcdesimp.landlord.persistantData.OwnedLand;
import org.bukkit.*;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by jcdesimp on 2/18/15.
 * List the friends of the current land
 */
public class Friends implements LandlordCommand, Listener {

    private Landlord plugin;

    public Friends(Landlord plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    private void showFriendsInv(Player p, int page, OwnedLand land) {
        List<UUID> friends = land.getFriends();
        boolean pages = friends.size() > 45;
        int firstFriend = 0;

        int maxPage;
        float nbPages = (float) friends.size() / 45;
        if (nbPages == friends.size() / 45)
            maxPage = friends.size() / 45;
        else
            maxPage = friends.size() / 45 + 1;

        if (pages) {
            if (page > maxPage)
                page = maxPage;
        } else
            page = 1;

        Inventory inv = Bukkit.createInventory(null, 54, "§rAmis ● Page " + page);

        if (pages) {
            for (int j = 1; j < page; j++)
                firstFriend = firstFriend + 45;
        }

        if (friends.size() == 0) {
            ItemStack item = new ItemStack(Material.BARRIER);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName("§cIl n'y a pas d'ami sur ce claim.");
            item.setItemMeta(itemMeta);
            inv.addItem(item);
        } else {
            int i;
            ArrayList<String> lore = new ArrayList<>();
            for (i = firstFriend; i < firstFriend + 45; i++) {
                UUID friend = friends.get(i);
                OfflinePlayer op = Bukkit.getOfflinePlayer(friend);
                ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
                SkullMeta meta = (SkullMeta) skull.getItemMeta();
                meta.setOwner(op.getName());
                meta.setDisplayName("§6" + op.getName());
                lore.clear();
                lore.add("");
                lore.add("§aClic pour supprimer des amis");
                lore.add("§8UUID : " + friend.toString());
                lore.add("§8Claim : " + land.getData().getX() + " " + land.getData().getZ());
                meta.setLore(lore);
                skull.setItemMeta(meta);
                inv.addItem(skull);

                if (i + 1 == friends.size())
                    break;
            }

            if (i + 1 != friends.size()) {
                ItemStack next = new ItemStack(Material.ARROW);
                ItemMeta meta = next.getItemMeta();
                meta.setDisplayName("§6Page suivante");
                next.setItemMeta(meta);
                inv.setItem(50, next);
            }

            if (page > 1) {
                ItemStack item = new ItemStack(Material.ARROW);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§6Page précédente");
                item.setItemMeta(meta);
                inv.setItem(48, item);
            }

            ItemStack claim = new ItemStack(Material.PAPER);
            ItemMeta meta = claim.getItemMeta();
            meta.setDisplayName("§6Chunk : X:" + land.getData().getX() + " Z:" + land.getData().getZ() + " | " + land.getFriends().size() + " amis");
            claim.setItemMeta(meta);
            inv.setItem(49, claim);
        }
        p.openInventory(inv);
    }

    @SuppressWarnings("deprecation")
    private void showConfirmFriendDelete(Player p, OwnedLand land, UUID friend) {
        Inventory inv = Bukkit.createInventory(null, 9, "§rConfirmer la suppression");

        ItemStack yes = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.LIME.getWoolData());
        ItemMeta yesMeta = yes.getItemMeta();
        yesMeta.setDisplayName("§aConfirmer la suppression");
        yes.setItemMeta(yesMeta);
        inv.setItem(2, yes);

        ItemStack no = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.RED.getWoolData());
        ItemMeta noMeta = no.getItemMeta();
        noMeta.setDisplayName("§cAnnuler la suppression");
        no.setItemMeta(noMeta);
        inv.setItem(6, no);

        ItemStack ami = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        SkullMeta meta = (SkullMeta) ami.getItemMeta();
        OfflinePlayer op = Bukkit.getOfflinePlayer(friend);
        meta.setDisplayName("§e" + op.getName());
        meta.setOwner(op.getName());
        ArrayList<String> lore = new ArrayList<>();
        lore.add("§8UUID : " + friend.toString());
        lore.add("§8Claim : " + land.getData().getX() + " " + land.getData().getZ());
        meta.setLore(lore);
        ami.setItemMeta(meta);
        inv.setItem(4, ami);

        p.openInventory(inv);
    }

    public boolean execute(CommandSender sender, String[] args, String label) {
        FileConfiguration messages = plugin.getMessageConfig();

        final String notPlayer = messages.getString("info.warnings.playerCommand");
        final String noPerms = messages.getString("info.warnings.noPerms");
        final String notOwner = messages.getString("info.warnings.notOwner");

        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.DARK_RED + notPlayer));
        } else {
            Player player = (Player) sender;
            if (!player.hasPermission("landlord.player.own")) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + noPerms));
                return true;
            }
            OwnedLand land = plugin.getLandManager().getLandFromCache(player.getLocation());
            if (land == null || (!land.getOwner().equals(player.getUniqueId()) && !player.hasPermission("landlord.admin.friends"))) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + notOwner));
                return true;
            }

            if (args.length > 1) {
                try {
                    int page = Integer.parseInt(args[1]);
                    showFriendsInv((Player) sender, page, land);
                } catch (NumberFormatException e) {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "§7[§6§lClaim >§7]§f Numéro de page invalide. /" + label + " friends [page]"));
                }
            } else {
                showFriendsInv((Player) sender, 1, land);
            }
        }
        return true;
    }

    @EventHandler
    @SuppressWarnings("deprecation")
    public void onInvClick(InventoryClickEvent e) {
        if (e.getInventory().getName().contains("§rAmis ● Page") && e.getSlotType() == InventoryType.SlotType.CONTAINER) {
            e.setCancelled(true);
            if (e.getSlot() < 54) {
                Player p = (Player) e.getWhoClicked();
                int page = Integer.parseInt(e.getInventory().getName().split("Page ")[1]);

                if (e.getSlot() == 50 && e.getCurrentItem().getType() == Material.ARROW)
                    p.performCommand("ll friends " + (page + 1));

                if (e.getSlot() == 48 && e.getCurrentItem().getType() == Material.ARROW)
                    p.performCommand("ll friends " + (page - 1));

                if (e.getCurrentItem().getType() == Material.SKULL_ITEM) {
                    ItemStack head = e.getCurrentItem();
                    ArrayList<String> lore = new ArrayList<>(head.getItemMeta().getLore());
                    UUID uuid = UUID.fromString(lore.get(2).replace("§8UUID : ", ""));
                    String coords = lore.get(3).split(" : ")[1];
                    OwnedLand land = plugin.getLandManager().getLandFromCache(Bukkit.getWorld("world").getChunkAt(Integer.parseInt(coords.split(" ")[0]), Integer.parseInt(coords.split(" ")[1])));
                    showConfirmFriendDelete(p, land, uuid);
                }
            }
        }

        if(e.getInventory().getName().equals("§rConfirmer la suppression") && e.getSlotType() == InventoryType.SlotType.CONTAINER){
            e.setCancelled(true);
            if(e.getSlot() < 9){
                if(e.getCurrentItem().getType() == Material.STAINED_GLASS_PANE){
                    ItemStack head = e.getInventory().getItem(4);
                    ArrayList<String> lore = new ArrayList<>(head.getItemMeta().getLore());
                    UUID uuid = UUID.fromString(lore.get(0).replace("§8UUID : ", ""));
                    String pName = head.getItemMeta().getDisplayName();
                    Player p = (Player) e.getWhoClicked();

                    String coords = lore.get(1).split(" : ")[1];
                    OwnedLand land = plugin.getLandManager().getLandFromCache(Bukkit.getWorld("world").getChunkAt(Integer.parseInt(coords.split(" ")[0]), Integer.parseInt(coords.split(" ")[1])));

                    if(e.getCurrentItem().getData().getData() == DyeColor.LIME.getWoolData()){
                        land.removeFriend(uuid);
                        Landlord.getInstance().getLandManager().insertOrReplaceIntoCache(land);
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', "§7[§6§lClaim >§7]§f Le joueur " + pName + "§f a été supprimé des amis de ce claim."));
                        showFriendsInv(p, 1, land);
                    } else {
                        showFriendsInv(p, 1, land);
                    }
                }
            }
        }
    }

    public String getHelpText(CommandSender sender) {
        FileConfiguration messages = plugin.getMessageConfig();

        final String usage = messages.getString("commands.friends.usage"); // get the base usage string
        final String desc = messages.getString("commands.friends.description");   // get the description

        // return the constructed and colorized help string
        return Utils.helpString(usage, desc, getTriggers()[0].toLowerCase());

    }

    public String[] getTriggers() {
        final List<String> triggers = plugin.getMessageConfig().getStringList("commands.friends.triggers");
        return triggers.toArray(new String[triggers.size()]);
    }
}
