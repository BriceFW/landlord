package com.jcdesimp.landlord.commands;

import com.jcdesimp.landlord.Landlord;
import com.jcdesimp.landlord.persistantData.OwnedLand;
import fr.vikingsworld.vikingsworld.Viking;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jcdesimp on 2/18/15.
 * Command to get a list of the sender player's land
 */
public class LandList implements LandlordCommand, Listener {

    private Landlord plugin;

    public LandList(Landlord plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @SuppressWarnings("deprecation")
    private void showConfirmDeleteInv(Player p, ItemStack claim) {
        Inventory inv = Bukkit.createInventory(null, 9, "§rSupprimer le claim");

        ItemStack yes = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.LIME.getWoolData());
        ItemMeta yesMeta = yes.getItemMeta();
        yesMeta.setDisplayName("§aConfirmer la suppression");
        yes.setItemMeta(yesMeta);
        inv.setItem(2, yes);

        ItemStack no = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.RED.getWoolData());
        ItemMeta noMeta = no.getItemMeta();
        noMeta.setDisplayName("§cAnnuler la suppression");
        no.setItemMeta(noMeta);
        inv.setItem(6, no);

        ItemMeta m = claim.getItemMeta();
        m.setLore(null);
        claim.setItemMeta(m);
        inv.setItem(4, claim);

        p.openInventory(inv);
    }

    private void showClaimListInv(Player p, int page) {
        int landsCount = plugin.getDb().getLandCount(p.getUniqueId());
        boolean pages = landsCount > 45;
        int firstClaim = 0;

        int maxPage;
        float nbPages = (float) landsCount / 45;
        if (nbPages == landsCount / 45)
            maxPage = landsCount / 45;
        else
            maxPage = landsCount / 45 + 1;

        if (pages) {
            if (page > maxPage)
                page = maxPage;
        } else
            page = 1;

        Inventory inv = Bukkit.createInventory(null, 54, "§rClaims ● Page " + page);

        if (pages) {
            for (int j = 1; j < page; j++)
                firstClaim = firstClaim + 45;
        }

        if (landsCount == 0) {
            ItemStack item = new ItemStack(Material.BARRIER);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName("§cVous n'avez pas de claim.");
            item.setItemMeta(itemMeta);
            inv.addItem(item);
        } else {
            List<OwnedLand> lands = plugin.getDb().getLands(p.getUniqueId(), 45, firstClaim);
            World world = Bukkit.getWorld("world");

            ArrayList<String> lore = new ArrayList<>();
            lore.add("§cCliquez pour unclaim ce chunk.");

            for (OwnedLand land : lands) {
                ItemStack claim = new ItemStack(Material.PAPER);
                ItemMeta meta = claim.getItemMeta();

                Chunk c = world.getChunkAt(land.getData().getX(), land.getData().getZ());
                Location l = c.getBlock(8, 0 ,8).getLocation();

                meta.setDisplayName("§6Claim : X:" + l.getBlockX() + " Z:" + l.getBlockZ() + " | " + land.getFriends().size() + " amis");
                meta.setLore(lore);
                claim.setItemMeta(meta);
                inv.addItem(claim);
            }

            if (lands.size() == 45 && page * 45 < landsCount) {
                ItemStack next = new ItemStack(Material.ARROW);
                ItemMeta meta = next.getItemMeta();
                meta.setDisplayName("§6Page suivante");
                next.setItemMeta(meta);
                inv.setItem(50, next);
            }

            if (page > 1) {
                ItemStack item = new ItemStack(Material.ARROW);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§6Page précédente");
                item.setItemMeta(meta);
                inv.setItem(48, item);
            }

            int orLimit = plugin.getConfig().getInt("limits.landLimit", 10);
            int limit = plugin.getConfig().getInt("limits.landLimit", 10);

            if (p.hasPermission("landlord.limit.extra6")) {
                limit = orLimit + plugin.getConfig().getInt("limits.extra6", 0);
            } else if (p.hasPermission("landlord.limit.extra5")) {
                limit = orLimit + plugin.getConfig().getInt("limits.extra5", 0);
            } else if (p.hasPermission("landlord.limit.extra4")) {
                limit = orLimit + plugin.getConfig().getInt("limits.extra4", 0);
            } else if (p.hasPermission("landlord.limit.extra3")) {
                limit = orLimit + plugin.getConfig().getInt("limits.extra3", 0);
            } else if (p.hasPermission("landlord.limit.extra2")) {
                limit = orLimit + plugin.getConfig().getInt("limits.extra2", 0);
            } else if (p.hasPermission("landlord.limit.extra")) {
                limit = orLimit + plugin.getConfig().getInt("limits.extra", 0);
            }

            limit += new Viking(p).getAdditionalClaim();

            ItemStack info = new ItemStack(Material.MAP);
            ItemMeta infoItemMeta = info.getItemMeta();
            infoItemMeta.setDisplayName("§6Vous possédez §a" + landsCount + "/" + limit + "§6 claims.");
            info.setItemMeta(infoItemMeta);
            inv.setItem(49, info);
        }

        p.openInventory(inv);
    }

    @EventHandler
    @SuppressWarnings("deprecation")
    public void onInvClick(InventoryClickEvent e) {
        if (e.getInventory().getName().contains("§rClaims") && e.getSlotType() == InventoryType.SlotType.CONTAINER) {
            e.setCancelled(true);
            if (e.getSlot() < 54) {
                Player p = (Player) e.getWhoClicked();
                int page = Integer.parseInt(e.getInventory().getName().split("Page ")[1]);

                if (e.getSlot() == 50 && e.getCurrentItem().getType() == Material.ARROW)
                    p.performCommand("ll list " + (page + 1));

                if (e.getSlot() == 48 && e.getCurrentItem().getType() == Material.ARROW)
                    p.performCommand("ll list " + (page - 1));

                if (e.getCurrentItem().getType() == Material.PAPER) {
                    showConfirmDeleteInv(p, e.getCurrentItem());
                }
            }
        }

        if (e.getInventory().getName().contains("§rSupprimer le claim") && e.getSlotType() == InventoryType.SlotType.CONTAINER) {
            e.setCancelled(true);
            if (e.getSlot() < 9) {
                Player p = (Player) e.getWhoClicked();
                if (e.getCurrentItem().getType() == Material.STAINED_GLASS_PANE) {
                    if (e.getCurrentItem().getData().getData() == DyeColor.LIME.getWoolData()) {
                        String coords = e.getInventory().getItem(4).getItemMeta().getDisplayName().split(" : ")[1].split(" \\| ")[0];
                        int x = Integer.parseInt(coords.split(" ")[0].replace("X:", ""));
                        int z = Integer.parseInt(coords.split(" ")[1].replace("Z:", ""));
                        Block b = Bukkit.getWorld("world").getBlockAt(x, 50, z);
                        plugin.getLandManager().getLandFromCache("world", b.getChunk().getX(), b.getChunk().getZ()).delete();
                        p.sendMessage("§7[§6§lClaim >§7]§f Vous avez unclaim le chunk (" + b.getChunk().getX() + ", " + b.getChunk().getZ() + ").");
                    }
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            showClaimListInv(p, 1);
                        }
                    }.runTaskLater(plugin, 2);
                }
            }
        }
    }

    /**
     * Display a list of all owned land to a player
     *
     * @param sender who executed the command
     * @param args   given with command
     * @return boolean
     */
    public boolean execute(CommandSender sender, String[] args, String label) {

        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "Commande réservée aux joueurs."));
            return false;
        }

        if (args.length > 1) {
            try {
                int page = Integer.parseInt(args[1]);
                showClaimListInv((Player) sender, page);
            } catch (NumberFormatException e) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "§7[§6§lClaim >§7]§f Numéro de page invalide. /" + label + " list [page]"));
            }
        } else {
            showClaimListInv((Player) sender, 1);
        }
        return true;
    }

    public String getHelpText(CommandSender sender) {
        FileConfiguration messages = plugin.getMessageConfig();
        final String usage = messages.getString("commands.landList.usage"); // get the base usage string
        final String desc = messages.getString("commands.landList.description");   // get the description
        // return the constructed and colorized help string
        return Utils.helpString(usage, desc, getTriggers()[0].toLowerCase());

    }

    public String[] getTriggers() {
        final List<String> triggers = plugin.getMessageConfig().getStringList("commands.landList.triggers");
        return triggers.toArray(new String[triggers.size()]);
    }
}
