package com.jcdesimp.landlord.commands;

import com.jcdesimp.landlord.Landlord;
import com.jcdesimp.landlord.persistantData.OwnedLand;
import fr.vikingsworld.vikingsworld.Viking;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by jcdesimp on 2/17/15.
 * LandlordCommand object that lets a user Claim land
 */
public class Claim implements LandlordCommand {


    private Landlord plugin;


    /**
     * Constructor for Claim command
     *
     * @param plugin the main Landlord plugin
     */
    public Claim(Landlord plugin) {
        this.plugin = plugin;
    }

    /**
     * Called when landlord claim command is executed
     * This command must be run by a player
     *
     * @param sender who executed the command
     * @param args   given with command
     * @return boolean
     */
    public boolean execute(CommandSender sender, String[] args, String label) {

        FileConfiguration messages = plugin.getMessageConfig();

        final String notPlayer = messages.getString("info.warnings.playerCommand");     // When run by non-player
        final String noPerms = messages.getString("info.warnings.noPerms");     // No permissions

        final String cannotClaim = messages.getString("info.warnings.noClaim");     // Claiming disabled in this world
        final String alreadyOwn = messages.getString("commands.claim.alerts.alreadyOwn");       // When you already own this land
        final String otherOwn = messages.getString("commands.claim.alerts.otherOwn");       // Someone else owns this land
        final String noClaimZone = messages.getString("commands.claim.alerts.noClaimZone");     // You can't claim here! (Worldguard)
        final String ownLimit = messages.getString("commands.claim.alerts.ownLimit");       // Chunk limit hit
        final String success = messages.getString("commands.claim.alerts.success");     // Chunk claim successful

        //is sender a player
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.DARK_RED + notPlayer));
        } else {
            Player player = (Player) sender;
            if (!player.hasPermission("landlord.player.own")) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + noPerms));
                return true;
            }


            //sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.GOLD + "Current Location: " + player.getLocation().toString());
            Chunk currChunk = player.getLocation().getChunk();

            List<String> disabledWorlds = plugin.getConfig().getStringList("disabled-worlds");
            for (String s : disabledWorlds) {
                if (s.equalsIgnoreCase(currChunk.getWorld().getName())) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + cannotClaim));
                    return true;
                }
            }

            // Check if worldguard is installed
            if (plugin.hasWorldGuard()) {
                // if it is make sure that the attempted land claim isn't with a protected worldguard region.
                if (!plugin.getWgHandler().canClaim(player, currChunk)) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + noClaimZone));
                    return true;
                }
            }

            OwnedLand land = plugin.getLandManager().getLandFromCache(player.getLocation());

            if (land != null) {
                //Check if they already own this land
                if (land.getOwner().equals(player.getUniqueId())) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.YELLOW + alreadyOwn));
                    return true;
                }
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.YELLOW + otherOwn));
                return true;
            }
            int orLimit = plugin.getConfig().getInt("limits.landLimit", 10);
            int limit = plugin.getConfig().getInt("limits.landLimit", 10);

            if (player.hasPermission("landlord.limit.extra6")) {
                limit = orLimit + plugin.getConfig().getInt("limits.extra6", 0);
            } else if (player.hasPermission("landlord.limit.extra5")) {
                limit = orLimit + plugin.getConfig().getInt("limits.extra5", 0);
            } else if (player.hasPermission("landlord.limit.extra4")) {
                limit = orLimit + plugin.getConfig().getInt("limits.extra4", 0);
            } else if (player.hasPermission("landlord.limit.extra3")) {
                limit = orLimit + plugin.getConfig().getInt("limits.extra3", 0);
            } else if (player.hasPermission("landlord.limit.extra2")) {
                limit = orLimit + plugin.getConfig().getInt("limits.extra2", 0);
            } else if (player.hasPermission("landlord.limit.extra")) {
                limit = orLimit + plugin.getConfig().getInt("limits.extra", 0);
            }

            limit += new Viking(player).getAdditionalClaim();

            if (limit >= 0 && !player.hasPermission("landlord.limit.override")) {
                if (plugin.getDb().getLandCount(player.getUniqueId()) >= limit) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + ownLimit.replace("#{limit}", "" + limit)));
                    return true;
                }
            }

            plugin.getLandManager().createNewLand(player.getUniqueId(), currChunk);

            OwnedLand.highlightLand(player, Particle.VILLAGER_HAPPY);
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                    ChatColor.GREEN + success
                            .replace("#{chunkCoords}", "(" + currChunk.getX() + ", " + currChunk.getZ() + ")")
                            .replace("#{worldName}", currChunk.getWorld().getName())));

            if (plugin.getConfig().getBoolean("options.soundEffects", true)) {
                player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_TWINKLE, 10, 10);
            }

            plugin.getMapManager().updateAll();
            //sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.DARK_GREEN + "Land claim command executed!");
        }
        return true;
    }

    public String getHelpText(CommandSender sender) {
        FileConfiguration messages = plugin.getMessageConfig();

        String usage = messages.getString("commands.claim.usage");                      // get the base usage string
        String desc = messages.getString("commands.claim.description");                      // get the description

        String helpString = ""; // start building the help string

        helpString += Utils.helpString(usage, desc, getTriggers()[0].toLowerCase());

        // return the constructed and colorized help string
        return helpString;

    }

    public String[] getTriggers() {
        List<String> triggers = plugin.getMessageConfig().getStringList("commands.claim.triggers");
        return triggers.toArray(new String[triggers.size()]);
    }
}
