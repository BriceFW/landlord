package com.jcdesimp.landlord.commands;

import com.jcdesimp.landlord.Landlord;
import com.jcdesimp.landlord.persistantData.OwnedLand;
import fr.vikingsworld.vikingsworld.Viking;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by jcdesimp on 2/18/15.
 * LandlordCommand to add a friend to a plot of land
 */
public class AddFriend implements LandlordCommand {

    private Landlord plugin;

    public AddFriend(Landlord plugin) {
        this.plugin = plugin;
    }

    /**
     * Adds a friend to an owned chunk
     * Called when landlord addfriend command is executed
     * This command must be run by a player
     *
     * @param sender who executed the command
     * @param args   given with command
     * @param label  base command executed
     * @return boolean
     */
    public boolean execute(CommandSender sender, String[] args, String label) {
        //is sender a player
        FileConfiguration messages = plugin.getMessageConfig();

        final String usage = messages.getString("commands.addFriend.usage");
        final String notPlayer = messages.getString("info.warnings.playerCommand");
        final String noPerms = messages.getString("info.warnings.noPerms");

        final String notOwner = messages.getString("info.warnings.notOwner");
        final String unknownPlayer = messages.getString("info.warnings.unknownPlayer");
        final String alreadyFriend = messages.getString("commands.addFriend.alerts.alreadyFriend");
        final String nowFriend = messages.getString("commands.addFriend.alerts.success");

        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.DARK_RED + notPlayer));
        } else {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + usage.replace("#{label}", label).replace("#{cmd}", args[0])));
                return true;
            }
            Player player = (Player) sender;
            if (!player.hasPermission("landlord.player.own")) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + noPerms));
                return true;
            }

            Chunk currChunk = player.getLocation().getChunk();

            OwnedLand land = plugin.getLandManager().getLandFromCache(currChunk.getWorld().getName(), currChunk.getX(), currChunk.getZ());

            //Does land exist, and if so does player own it
            if (land == null || (!land.getOwner().equals(player.getUniqueId()) && !player.hasPermission("landlord.admin.modifyfriends"))) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + notOwner));
                return true;
            }
            //
            Viking possible = Viking.getViking(args[1]);
            if (possible == null) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + unknownPlayer));
                return true;
            }

            if (!land.addFriend(possible.getUniqueId())) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.YELLOW + alreadyFriend.replace("#{player}", possible.getName())));
                return true;
            }
            if (plugin.getConfig().getBoolean("options.particleEffects", true)) {      //conf
                OwnedLand.highlightLand(player, Particle.HEART, 2);
            }

            Landlord.getInstance().getLandManager().insertOrReplaceIntoCache(land);
            if (plugin.getConfig().getBoolean("options.soundEffects", true)) {     //conf
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 10, .2f);
            }
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.GREEN + nowFriend.replace("#{player}", possible.getName())));
            plugin.getMapManager().updateAll();
        }
        return true;
    }

    public String getHelpText(CommandSender sender) {
        FileConfiguration messages = plugin.getMessageConfig();

        final String usage = messages.getString("commands.addFriend.usage"); // get the base usage string
        final String desc = messages.getString("commands.addFriend.description");   // get the description

        // return the constructed and colorized help string
        return Utils.helpString(usage, desc, getTriggers()[0].toLowerCase());
    }

    public String[] getTriggers() {
        final List<String> triggers = plugin.getMessageConfig().getStringList("commands.addFriend.triggers");
        return triggers.toArray(new String[triggers.size()]);
    }
}
