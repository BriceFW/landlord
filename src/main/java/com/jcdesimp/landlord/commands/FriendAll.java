package com.jcdesimp.landlord.commands;

import com.jcdesimp.landlord.Landlord;
import com.jcdesimp.landlord.persistantData.OwnedLand;
import fr.vikingsworld.vikingsworld.Viking;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
/**
 * Created by jcdesimp on 2/18/15.
 * Command for a player toi add a friend to all of their land at once.
 */
public class FriendAll implements LandlordCommand {

    private Landlord plugin;

    public FriendAll(Landlord plugin) {
        this.plugin = plugin;
    }

    public boolean execute(CommandSender sender, String[] args, String label) {
        FileConfiguration messages = plugin.getMessageConfig();
        final String notPlayerString = messages.getString("info.warnings.playerCommand");
        final String usageString = messages.getString("commands.friendAll.usage");
        final String noPermsString = messages.getString("info.warnings.noPerms");
        final String unknownPlayer = messages.getString("info.warnings.unknownPlayer");
        final String friendAddedString = messages.getString("commands.friendAll.alerts.success");
        final String noLandString = messages.getString("commands.friendAll.alerts.noLand");


        //is sender a player
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.DARK_RED + notPlayerString));
        } else {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + usageString.replace("#{label}", label)));
                return true;
            }
            Player player = (Player) sender;
            if (!player.hasPermission("landlord.player.own")) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + noPermsString));
                return true;
            }

            Viking possible = Viking.getViking(args[1]);
            if (possible == null) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + unknownPlayer));
                return true;
            }

            int landCount = plugin.getDb().getLandCount(((Player) sender).getUniqueId());
            if (landCount > 0) {
                player.sendMessage("§7[§6§lClaim >§7]§f Ajout de " + possible.getName() + " en ami à tous vos claims ...");
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        List<OwnedLand> pLand = plugin.getDb().getLands(((Player) sender).getUniqueId());
                        for (OwnedLand l : pLand) {
                            l.addFriend(possible.getUniqueId());
                            Landlord.getInstance().getLandManager().insertOrReplaceIntoCache(l);
                        }
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.GREEN + friendAddedString.replace("#{player}", possible.getName())));
                            }
                        }.runTask(plugin);
                    }
                }.runTaskAsynchronously(plugin);
                return true;
            } else {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.YELLOW + noLandString));
            }

        }
        return true;
    }

    public String getHelpText(CommandSender sender) {
        FileConfiguration messages = plugin.getMessageConfig();

        final String usage = messages.getString("commands.friendAll.usage"); // get the base usage string
        final String desc = messages.getString("commands.friendAll.description");   // get the description

        // return the constructed and colorized help string
        return Utils.helpString(usage, desc, getTriggers()[0].toLowerCase());

    }

    public String[] getTriggers() {
        final List<String> triggers = plugin.getMessageConfig().getStringList("commands.friendAll.triggers");
        return triggers.toArray(new String[triggers.size()]);
    }
}
