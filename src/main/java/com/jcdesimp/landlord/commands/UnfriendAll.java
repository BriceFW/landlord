package com.jcdesimp.landlord.commands;

import com.jcdesimp.landlord.Landlord;
import com.jcdesimp.landlord.persistantData.OwnedLand;
import fr.vikingsworld.vikingsworld.Viking;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

/**
 * Created by jcdesimp on 2/18/15.
 * Command for a user to remove a friend from all land at once.
 */
public class UnfriendAll implements LandlordCommand {

    private Landlord plugin;

    public UnfriendAll(Landlord plugin) {
        this.plugin = plugin;
    }

    public boolean execute(CommandSender sender, String[] args, String label) {

        FileConfiguration messages = plugin.getMessageConfig();

        final String notPlayer = messages.getString("info.warnings.playerCommand");
        final String usage = messages.getString("commands.unfriendAll.usage");
        final String noPerms = messages.getString("info.warnings.noPerms");
        final String unknownPlayer = messages.getString("info.warnings.unknownPlayer");
        final String playerRemoved = messages.getString("commands.unfriendAll.alerts.playerRemoved");
        final String noLand = messages.getString("commands.unfriendAll.alerts.noLand");

        //is sender a player
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.DARK_RED + notPlayer));
        } else {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + usage.replace("#label}", label).replace("#{command}", args[0])));
                return true;
            }
            Player player = (Player) sender;
            if (!player.hasPermission("landlord.player.own")) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + noPerms));
                return true;
            }

            Viking possible = Viking.getViking(args[1]);
            if (possible == null) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + unknownPlayer));
                return true;
            }
            int landCount = plugin.getDb().getLandCount(player.getUniqueId());

            if (landCount > 0) {
                player.sendMessage("§7[§6§lClaim >§7]§f Suppression de " + possible.getName() + " des amis de tous vos claims ...");
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        List<OwnedLand> pLand = plugin.getDb().getLands(player.getUniqueId());
                        for (OwnedLand l : pLand) {
                            l.removeFriend(possible.getUniqueId());
                            Landlord.getInstance().getLandManager().insertOrReplaceIntoCache(l);
                        }
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.GREEN + playerRemoved.replace("#{playername}", possible.getName())));
                            }
                        }.runTask(plugin);
                    }
                }.runTaskAsynchronously(plugin);
                return true;
            } else {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.YELLOW + noLand));
            }

        }
        return true;
    }

    public String getHelpText(CommandSender sender) {

        FileConfiguration messages = plugin.getMessageConfig();

        final String usage = messages.getString("commands.unfriendAll.usage"); // get the base usage string
        final String desc = messages.getString("commands.unfriendAll.description");   // get the description

        // return the constructed and colorized help string
        return Utils.helpString(usage, desc, getTriggers()[0].toLowerCase());

    }

    public String[] getTriggers() {
        final List<String> triggers = plugin.getMessageConfig().getStringList("commands.unfriendAll.triggers");
        return triggers.toArray(new String[triggers.size()]);
    }
}
