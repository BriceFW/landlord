package com.jcdesimp.landlord.commands;

import com.jcdesimp.landlord.Landlord;
import com.jcdesimp.landlord.persistantData.OwnedLand;
import fr.vikingsworld.vikingsworld.Viking;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.UUID;

/**
 * Created by jcdesimp on 2/18/15.
 * List the land of a specified player (typically for administrator use)
 */
public class ListPlayer implements LandlordCommand, Listener {

    private Landlord plugin;

    public ListPlayer(Landlord plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    private void showClaimListInv(Player p, int page, UUID owner) {
        int landsCount = plugin.getDb().getLandCount(owner);
        boolean pages = landsCount > 45;
        int firstClaim = 0;

        int maxPage;
        float nbPages = (float) landsCount / 45;
        if (nbPages == landsCount / 45)
            maxPage = landsCount / 45;
        else
            maxPage = landsCount / 45 + 1;

        if (pages) {
            if (page > maxPage)
                page = maxPage;
        } else
            page = 1;

        Viking v = new Viking(owner);
        Inventory inv = Bukkit.createInventory(null, 54, "§cClaims ● Page " + page);

        if (pages) {
            for (int j = 1; j < page; j++)
                firstClaim = firstClaim + 45;
        }

        if (landsCount == 0) {
            ItemStack item = new ItemStack(Material.BARRIER);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName("§c" + v.getName() + " n'a pas de claim.");
            item.setItemMeta(itemMeta);
            inv.addItem(item);
        } else {
            List<OwnedLand> lands = plugin.getDb().getLands(owner, 45, firstClaim);

            for (OwnedLand land : lands) {
                ItemStack claim = new ItemStack(Material.PAPER);
                ItemMeta meta = claim.getItemMeta();

                meta.setDisplayName("§6Chunk : X:" + land.getData().getX() + " Z:" + land.getData().getZ() + " | " + land.getFriends().size() + " amis");
                claim.setItemMeta(meta);
                inv.addItem(claim);
            }

            if (lands.size() == 45 && page * 45 < landsCount) {
                ItemStack next = new ItemStack(Material.ARROW);
                ItemMeta meta = next.getItemMeta();
                meta.setDisplayName("§6Page suivante");
                next.setItemMeta(meta);
                inv.setItem(50, next);
            }

            if (page > 1) {
                ItemStack item = new ItemStack(Material.ARROW);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§6Page précédente");
                item.setItemMeta(meta);
                inv.setItem(48, item);
            }

            ItemStack info = new ItemStack(Material.MAP);
            ItemMeta infoItemMeta = info.getItemMeta();
            infoItemMeta.setDisplayName("§6" + v.getName() + " possède §a" + landsCount + "§6 claims.");
            info.setItemMeta(infoItemMeta);
            inv.setItem(49, info);
        }

        p.openInventory(inv);
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent e) {
        if (e.getInventory().getName().contains("§cClaims")) {
            e.setCancelled(true);
            if (e.getSlot() < 54) {
                Player p = (Player) e.getWhoClicked();
                String playerName = e.getInventory().getItem(49).getItemMeta().getDisplayName().replace("§6", "").split(" possède")[0];
                int page = Integer.parseInt(e.getInventory().getName().split("Page ")[1]);

                if (e.getSlot() == 50 && e.getCurrentItem().getType() == Material.ARROW)
                    p.performCommand("ll listplayer " + playerName + " " + (page + 1));

                if (e.getSlot() == 48 && e.getCurrentItem().getType() == Material.ARROW)
                    p.performCommand("ll listplayer " + playerName + " " + (page - 1));

                if (e.getCurrentItem().getType() == Material.PAPER) {
                    String[] coords = e.getCurrentItem().getItemMeta().getDisplayName().replace("§6Chunk : ", "").split(" ");
                    int x = Integer.parseInt(coords[0].replace("X:", ""));
                    int z = Integer.parseInt(coords[1].replace("Z:", ""));
                    World w = Bukkit.getWorld("world");
                    Chunk c = w.getChunkAt(x, z);
                    Block b = c.getBlock(8, 0, 8);
                    int zb = w.getHighestBlockYAt(b.getX(), b.getZ());
                    Location loc = new Location(w, b.getX(), zb + 10, b.getZ());
                    p.teleport(loc);
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', "§7[§6§lClaim >§7]§f Téléportation vers le chunk X: " + x + ", Z: " + z));
                    p.closeInventory();
                }
            }
        }
    }

    /**
     * @param sender who sent the command
     * @param args   array of arguments given with the command
     * @param label  the actual command/alias that was entered.
     * @return Boolean of success
     */
    public boolean execute(CommandSender sender, String[] args, String label) {

        FileConfiguration messages = plugin.getMessageConfig();

        final String usage = messages.getString("commands.listPlayer.usage");
        final String noPerms = messages.getString("info.warnings.noPerms");
        final String ownsNone = messages.getString("commands.listPlayer.alerts.ownsNone");

        String owner;

        if (args.length > 1) {
            owner = args[1];
        } else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + usage.replace("#{label}", label)));
            return true;
        }

        if (!sender.hasPermission("landlord.admin.list")) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.RED + noPerms));
            return true;
        }

        Viking possible = Viking.getViking(owner);
        if (possible == null) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.YELLOW + ownsNone.replace("#{owner}", owner)));
            return true;
        }

        if (args.length > 2) {
            try {
                int page = Integer.parseInt(args[2]);
                showClaimListInv((Player) sender, page, possible.getUniqueId());
            } catch (NumberFormatException e) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "§7[§6§lClaim >§7]§f Numéro de page invalide. /" + label + " listplayer <joueur> [page]"));
            }
        } else {
            showClaimListInv((Player) sender, 1, possible.getUniqueId());
        }
        return true;
    }

    public String getHelpText(CommandSender sender) {

        if (!sender.hasPermission("landlord.admin.list")) {   // Don't bother showing command help if player can't do it
            return null;
        }

        FileConfiguration messages = plugin.getMessageConfig();

        final String usage = messages.getString("commands.listPlayer.usage");       // get the base usage string
        final String desc = messages.getString("commands.listPlayer.description");      // get the description

        // return the constructed and colorized help string
        return Utils.helpString(usage, desc, getTriggers()[0].toLowerCase());

    }

    public String[] getTriggers() {
        final List<String> triggers = plugin.getMessageConfig().getStringList("commands.listPlayer.triggers");
        return triggers.toArray(new String[triggers.size()]);
    }
}
