package com.jcdesimp.landlord.commands;

import com.jcdesimp.landlord.Landlord;
import fr.vikingsworld.vikingsworld.Viking;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BriceFW on 27/06/2017.
 * Copyright FunWise.
 */
public class Shop implements LandlordCommand, Listener {
    private Landlord pl;

    public Shop(Landlord plugin) {
        this.pl = plugin;
        pl.getServer().getPluginManager().registerEvents(this, pl);
    }

    @EventHandler
    @SuppressWarnings("deprecation")
    public void onInvClick(InventoryClickEvent e) {
        if (e.getInventory().getName().contains("§rBoutique de claims") && e.getSlotType() == InventoryType.SlotType.CONTAINER) {
            e.setCancelled(true);
            if (e.getSlot() < 9) {
                if (e.getCurrentItem().getType() == Material.IRON_HOE) {
                    if (e.getCurrentItem().getItemMeta().getLore().get(0).contains("§c")) {
                        e.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "§7[§6§lClaim >§7]§f Vous n'avez pas assez d'argent pour acheter ce pack."));
                    } else {
                        showConfirmInv((Player) e.getWhoClicked(), e.getCurrentItem());
                    }
                }
                if (e.getCurrentItem().getType() == Material.STAINED_GLASS_PANE) {
                    Player p = (Player) e.getWhoClicked();
                    if (e.getCurrentItem().getData().getData() == DyeColor.LIME.getWoolData()) {
                        ItemStack pack = e.getInventory().getItem(4);
                        int amount = Integer.parseInt(pack.getItemMeta().getDisplayName().split("de ")[1].split(" claims")[0]);
                        int price = Integer.parseInt(pack.getItemMeta().getDisplayName().split(" : ")[1].replace("$", ""));

                        if (pl.getEcon().getBalance(p) < price) {
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', "§7[§6§lClaim >§7]§f Vous n'avez pas assez d'argent pour acheter ce pack."));
                        } else {
                            pl.getEcon().withdrawPlayer(p, price);
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', "§7[§6§lClaim >§7]§f Vous avez acheté le pack de " + amount + " claims pour " + price + "$."));
                            Viking v = new Viking(p);
                            v.setAdditionalClaims(v.getAdditionalClaim() + amount);
                        }
                        p.closeInventory();
                    } else {
                        showShopInv(p);
                    }
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void showConfirmInv(Player p, ItemStack item) {
        Inventory inv = Bukkit.createInventory(null, 9, "§rBoutique de claims");

        ItemStack yes = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.LIME.getWoolData());
        ItemMeta yesMeta = yes.getItemMeta();
        yesMeta.setDisplayName("§aConfirmer l'achat");
        yes.setItemMeta(yesMeta);
        inv.setItem(2, yes);

        ItemStack no = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.RED.getWoolData());
        ItemMeta noMeta = no.getItemMeta();
        noMeta.setDisplayName("§cAnnuler l'achat");
        no.setItemMeta(noMeta);
        inv.setItem(6, no);

        ItemMeta meta = item.getItemMeta();
        meta.setLore(null);
        item.setItemMeta(meta);
        inv.setItem(4, item);

        p.openInventory(inv);
    }

    private void showShopInv(Player p) {
        Inventory inv = Bukkit.createInventory(null, 9, "§rBoutique de claims");
        List<String> offres = pl.getConfig().getStringList("claim-shop");
        for (String s : offres) {
            ItemStack offre = new ItemStack(Material.IRON_HOE);
            ItemMeta offreMeta = offre.getItemMeta();
            String amout = s.split(" ")[0];
            String price = s.split(" ")[1];
            offreMeta.setDisplayName("§6Pack de " + amout + " claims : " + price + "$");
            ArrayList<String> lore = new ArrayList<>();
            if (pl.getEcon().getBalance(p) < Integer.parseInt(price)) {
                lore.add("§cVous n'avez pas assez d'argent.");
            } else {
                lore.add("§aCliquez pour l'acheter.");
            }
            offreMeta.setLore(lore);
            offre.setItemMeta(offreMeta);
            inv.addItem(offre);
        }
        p.openInventory(inv);
    }


    @Override
    public boolean execute(CommandSender sender, String[] args, String label) {
        FileConfiguration messages = pl.getMessageConfig();
        final String notPlayer = messages.getString("info.warnings.playerCommand");

        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.DARK_RED + notPlayer));
        } else {
            Player player = (Player) sender;
            if (!player.hasPermission("landlord.shop")) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', "§7[§6§lClaim >§7]§f Le shop est accessible à partir du grade 7."));
                return true;
            }

            showShopInv(player);
        }
        return true;
    }

    @Override
    public String getHelpText(CommandSender sender) {
        return "§3/#{label} shop §f- Ouvrir le shop de claims";
    }

    @Override
    public String[] getTriggers() {
        return new String[]{"shop"};
    }
}
