package com.jcdesimp.landlord.persistantData;

import com.jcdesimp.landlord.Landlord;
import com.jcdesimp.landlord.landManagement.Landflag;
import org.bukkit.*;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class OwnedLand {

    private List<UUID> friends;
    private List<LandFlag> flags;
    private int landid;
    private UUID owner;
    private Data data;

    public OwnedLand(Data data) {
        this.data = data;
        this.friends = new ArrayList<>();
    }

    public int getId() {
        return landid;
    }

    public void setId(int id) {
        this.landid = id;
    }

    public UUID getOwner() {
        return owner;
    }

    public void setOwner(UUID ownerUUID) {
        this.owner = ownerUUID;
    }

    public String getOwnerUsername() {
        Player p = Bukkit.getPlayer(owner);
        if (p != null) {
            return p.getName();
        }
        OfflinePlayer op = Bukkit.getOfflinePlayer(owner);
        if (!op.hasPlayedBefore() && !op.isOnline())
            return ChatColor.ITALIC + "Inconnu";
        return op.getName();
    }

    public List<UUID> getFriends() {
        return friends;
    }

    public void setFriends(List<UUID> friends) {
        this.friends = friends;
    }

    public void setFlags(List<LandFlag> flags) {
        this.flags = flags;
    }

    public Data getData() {
        return data;
    }

    public Chunk getChunk() {
        return Bukkit.getWorld(data.getWorld()).getChunkAt(data.getX(), data.getZ());
    }

    public boolean hasPermTo(Player player, Landflag lf) {
        if (player.hasPermission("landlord.admin.bypass") || player.getUniqueId().equals(owner))
            return true;
        if (isFriend(player.getUniqueId()))
            return getFlag(lf).canFriends();
        return getFlag(lf).canEveryone();
    }

    public LandFlag getFlag(Landflag flagin) {
        for (LandFlag flag : flags) {
            if (flag.getIdentifier().equals(flagin.getClass().getSimpleName()))
                return flag;
        }
        return null;
    }

    public boolean canEveryone(Landflag lf) {
        return getFlag(lf).canEveryone();
    }

    public boolean addFriend(UUID f) {
        if (!isFriend(f)) {
            Landlord.getInstance().getDb().addFriend(landid, f);
            friends.add(f);
            return true;
        }
        return false;
    }

    public boolean removeFriend(UUID id) {
        if (isFriend(id)) {
            Landlord.getInstance().getDb().removeFriend(landid, id);
            friends.remove(id);
            return true;
        }
        return false;
    }

    public boolean isFriend(UUID id) {
        return friends.contains(id);
    }

    public void delete() {
        Landlord.getInstance().getDb().removeLand(landid);
        Landlord.getInstance().getLandManager().removeFromCache(data);
    }

    public static void highlightLand(Player p, Particle e) {
        highlightLand(p, e, 5);
    }

    public static void highlightLand(Player p, Particle e, int amt) {
        if (!Landlord.getInstance().getConfig().getBoolean("options.particleEffects", true)) {
            return;
        }
        Chunk chunk = p.getLocation().getChunk();
        ArrayList<Location> edgeBlocks = new ArrayList<>();
        for (int i = 0; i < 16; i++) {
            for (int ii = -1; ii <= 10; ii++) {
                edgeBlocks.add(chunk.getBlock(i, (int) (p.getLocation().getY()) + ii, 15).getLocation());
                edgeBlocks.add(chunk.getBlock(i, (int) (p.getLocation().getY()) + ii, 0).getLocation());
                edgeBlocks.add(chunk.getBlock(0, (int) (p.getLocation().getY()) + ii, i).getLocation());
                edgeBlocks.add(chunk.getBlock(15, (int) (p.getLocation().getY()) + ii, i).getLocation());
            }
        }

        for (Location edgeBlock : edgeBlocks) {
            edgeBlock.setZ(edgeBlock.getBlockZ() + .5);
            edgeBlock.setX(edgeBlock.getBlockX() + .5);
            p.getWorld().spawnParticle(e, edgeBlock, amt, 0.2, 0.2, 0.2, 20.0);
        }
    }

    public List<LandFlag> getFlags() {
        return flags;
    }

    public void save() {
        Landlord.getInstance().getDb().save(this);
    }
}
