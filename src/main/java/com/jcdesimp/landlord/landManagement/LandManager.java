package com.jcdesimp.landlord.landManagement;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.jcdesimp.landlord.Landlord;
import com.jcdesimp.landlord.persistantData.Data;
import com.jcdesimp.landlord.persistantData.LandFlag;
import com.jcdesimp.landlord.persistantData.OwnedLand;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class LandManager implements Listener {

    private LoadingCache<Data, OwnedLand> cache = CacheBuilder.newBuilder()
            .maximumSize(Landlord.getInstance().getConfig().getInt("cacheSize"))
            .build(new CacheLoader<Data, OwnedLand>() {
                @Override
                public OwnedLand load(Data data) throws Exception {
                    OwnedLand ownedLand = Landlord.getInstance().getDb().getLand(data);
                    if (ownedLand != null)
                        return ownedLand;
                    else
                        throw new Exception("Land ist not owned!");
                }
            });

    public void createNewLand(UUID owner, Chunk c) {
        Data data = new Data(c.getWorld().getName(), c.getX(), c.getZ());
        OwnedLand lnd = new OwnedLand(data);
        lnd.setOwner(owner);
        lnd.setId(Landlord.getInstance().getDb().getFirstFreeLandID());
        lnd.setFlags(getDefaultFlags(lnd.getId()));
        lnd.setFriends(new ArrayList<>());
        cache.put(data, lnd);
        Landlord.getInstance().getDb().addLand(lnd);
    }

    public OwnedLand getLandFromCache(Location l) {
        return getLandFromCache(l.getWorld().getName(), l.getChunk().getX(), l.getChunk().getZ());
    }

    public OwnedLand getLandFromCache(Chunk chunk) {
        return getLandFromCache(chunk.getWorld().getName(), chunk.getX(), chunk.getZ());
    }

    public OwnedLand getLandFromCache(String worldName, int x, int z) {
        Data data = new Data(worldName, x, z);
        try {
            return cache.get(data);
        } catch (ExecutionException ignored) {
            return null;
        }
    }

    private static Set<String> flags = Landlord.getInstance().getFlagManager().getRegisteredFlags().keySet();

    public static List<LandFlag> getDefaultFlags(int landid) {
        List<LandFlag> list = new ArrayList<>();

        for (String identifier : flags) {
            LandFlag flag = new LandFlag(landid, identifier, false, true);
            list.add(flag);
        }
        return list;
    }

    public void removeFromCache(Data data) {
        cache.invalidate(data);
    }

    public void insertOrReplaceIntoCache(OwnedLand land) {
        cache.put(land.getData(), land);
    }
}
