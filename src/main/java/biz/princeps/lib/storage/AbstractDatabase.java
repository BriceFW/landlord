package biz.princeps.lib.storage;

import com.jcdesimp.landlord.Landlord;
import com.jcdesimp.landlord.persistantData.Data;
import com.jcdesimp.landlord.persistantData.OwnedLand;
import org.bukkit.Location;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public abstract class AbstractDatabase {

    protected ExecutorService pool;
    private Logger logger;

    AbstractDatabase() {
        this.pool = Executors.newCachedThreadPool();
        logger = Landlord.getInstance().getLogger();
    }

    Logger getLogger() {
        return logger;
    }

    protected abstract void setupDatabase();

    public abstract void close();

    public abstract void execute(String query);

    public abstract OwnedLand getLand(Data data);

    protected abstract List<UUID> getFriends(int id);

    public abstract void removeFriend(int landid, UUID f);

    public abstract void addFriend(int landid, UUID f);

    public abstract void addLand(OwnedLand land);

    public abstract void removeLand(int landid);

    public abstract void save(OwnedLand land);

    public abstract List<OwnedLand> getLands(UUID owner);

    public abstract List<OwnedLand> getLands(UUID owner, int amount, int start);

    public abstract List<OwnedLand> getLands(UUID owner, String world);

    public abstract List<OwnedLand> getLands(String world);

    public abstract List<OwnedLand> getNearbyLands(Location location);

    public abstract int getLandCount(UUID owner);

    public abstract int getFirstFreeLandID();
}
