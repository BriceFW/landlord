package biz.princeps.lib.storage;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class MySQL extends AbstractDatabase {
    private HikariDataSource ds;

    public MySQL(String hostname, int port, String database, String username, String password) {
        super();
        HikariConfig config = new HikariConfig();
        config.setDataSourceClassName(MysqlDataSource.class.getName());

        config.addDataSourceProperty("serverName", hostname);
        config.addDataSourceProperty("port", port);
        config.addDataSourceProperty("databaseName", database);
        config.addDataSourceProperty("user", username);
        config.addDataSourceProperty("password", password);

        ds = new HikariDataSource(config);
    }

    public void setupDatabase() {

    }

    protected Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    public void close() {
        ds.close();
    }

    public void execute(String query) {
        pool.submit(() -> {
            try (Connection con = ds.getConnection();
                 PreparedStatement st = con.prepareStatement(query)) {
                st.execute();

            } catch (SQLException ignored) {
            }

        });
    }
}
